﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaddiesHurt : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.CompareTag("Black"))
        {
            GameManager.instance.killPlayer();
            Destroy(hit.gameObject);
        }
    }
}
