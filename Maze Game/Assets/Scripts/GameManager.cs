﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

public class GameManager : MonoBehaviour
{
    public GameObject[] mushLives;
    public static GameManager instance;
    public GameObject player;
    int playerLives;

    VignetteAndChromaticAberration chromatic_Vignette;

    // Use this for initialization
    void Start ()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        playerLives = 3;


        for (int i = 0; i < mushLives.Length; i ++) 
        {
            mushLives[i].SetActive(true);
        }

        chromatic_Vignette = Camera.main.GetComponent<VignetteAndChromaticAberration>();

    }

    // Update is called once per frame
    void Update ()
    {
        // Trippy effect
        chromatic_Vignette.intensity = Mathf.PingPong(Time.time, 7);
        chromatic_Vignette.blurSpread = Mathf.PingPong(Time.time, 5);
    }
    public void killPlayer()
    {
        playerLives--; // Takes away a life of the player

        if (playerLives == 0)
        {
            SceneManager.LoadScene("Lose"); //Loads the Lose scene
        }
        mushLives[playerLives].SetActive(false);
        player.transform.position = Checkpoint.checkPoint; // checkpoints
    }

}
