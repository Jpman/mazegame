﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBackgroundSound : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("musicVolume", 1.0f);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
