﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushBullet : MonoBehaviour
{
    public GameObject baddie;
    private Transform tf;

	// Use this for initialization
	void Start ()
    {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
    }

    void shootBaddie()
    {
        GameObject newBaddie = Instantiate(baddie, tf.position + Vector3.up * 10 + tf.forward, Quaternion.identity); // make the bullets
        newBaddie.GetComponent<Rigidbody>().AddForce((GameManager.instance.player.transform.position - (tf.position + Vector3.up * 10)) * 1000);
        
        Destroy(newBaddie, 10f);
    }

    public void startShoot()
    {
        InvokeRepeating("shootBaddie", 3f, 3f); //to shoot, 3s delay, 3s interval
    }

    public void stopShoot()
    {
        CancelInvoke("shootBaddie"); // stop shoot
    }

}
