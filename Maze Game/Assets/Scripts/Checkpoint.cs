﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{

    public static Vector3 checkPoint;

	// Use this for initialization
	void Start ()
    {
        checkPoint = GameManager.instance.player.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        checkPoint = transform.position;
    }
}
