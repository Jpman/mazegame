﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.None; // keeps the cursor locked in the center screen
        Cursor.visible = true;
	}
	
	// Update is called once per frame
	void Update () {


    }

    public void startGame()
    {
        SceneManager.LoadScene("MushTrip"); // main game scene
    }

    public void quitGame()
    {
        Application.Quit(); // Quits out of the game
    }

    public void mainMenu()
    {
        SceneManager.LoadScene("MainMenu"); // main menu scene
    }
    public void options()
    {
        SceneManager.LoadScene("Options");//options screen
    }

}
