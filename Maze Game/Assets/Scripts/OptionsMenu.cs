﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public Slider music;
    public Slider soundFX;

	// Use this for initialization
	void Start ()
    {
        music.value = PlayerPrefs.GetFloat("musicVolume", 1.0f);
        soundFX.value = PlayerPrefs.GetFloat("SFXVolume", 1.0f);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void backToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void updateMusic(float volume)
    {
        PlayerPrefs.SetFloat("musicVolume", volume);
    }

    public void updateSFX(float volume)
    {
        PlayerPrefs.SetFloat("SFXVolume", volume);
    }
}
