﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPLayerSound : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
	    GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("SFXVolume", 1.0f);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
