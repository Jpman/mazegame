﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    private Transform target;
    private Transform player;
    // Use this for initialization
    void Start()
    {
        target = GetComponent<Transform>();
        player = GameManager.instance.player.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
       
            target.LookAt(player);
    }
}